# Triangle

## is_triangle Function

### Description
Create a Python function named `is_triangle` that checks if a valid triangle with a nonzero area can be constructed using three numeric values, denoted as `a`, `b`, and `c`, representing the lengths of the sides.

### Function Signature

```python
def is_triangle(a, b, c):
    if a + b > c and a + c > b and b + c > a:
        return True
    else:
        return False   
    """
    Determine if a valid triangle with nonzero area can be constructed with the given side lengths.
    
    :param a: Length of the first side.
    :param b: Length of the second side.
    :param c: Length of the third side.
    
    :return: True if a triangle can be formed; False otherwise.
    """
